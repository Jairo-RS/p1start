package edu.uprm.cse.datastructures.cardealer.util;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class CircularSortedDoublyLinkedList<E> implements SortedList<E> {

	private int size;
	private Node<E> header;
	private Comparator<E> comp;
	
	/* Constructor */
	public CircularSortedDoublyLinkedList(Comparator<E> comparator) {
		this.header = new Node<E>(this.header, null, this.header);
		this.size = 0;
		comp = comparator;	
	}
	
	@Override
	public Iterator<E> iterator() {
		List<E> list = new ArrayList<E>();
		Node<E> current = this.header.getNext();
		
		while(current != this.header) {
			list.add(current.getElement());
			current = current.getNext();
		}
		
		return list.iterator();
	}

	/* Adds the element to the list. Uses the comparator to check where the element
	 * will go in the list. Returns true if the element is added.
	 */
	@Override
	public boolean add(E obj) {
		Node<E> newNode = new Node<E>(obj);
		
		if(this.isEmpty()) {
			header.setNext(newNode);
			header.setPrev(newNode);
			newNode.setNext(header);
			newNode.setPrev(header);
			size++;
			return true;
		} else {	
			Node<E> currentNode = header.getNext();
			while(currentNode != header) {
				if(comp.compare(currentNode.getElement(), obj) >= 0) {
					newNode.setNext(currentNode);
					newNode.setPrev(currentNode.getPrev());
					currentNode.getPrev().setNext(newNode);
					currentNode.setPrev(newNode);
					size++;
					return true;
				}
				currentNode = currentNode.getNext();
			}
			//Last car to add
			newNode.setNext(header);
			newNode.setPrev(header.getPrev());
			header.setPrev(newNode);
			newNode.getPrev().setNext(newNode);
			size++;
			return true;
		}
	}

	/* Returns the size of the list.*/
	@Override
	public int size() {
		return this.size;
	}

	/* Returns true if element is removed from the list.*/
	@Override
	public boolean remove(E obj) {
		if (obj == null) {
			throw new IllegalArgumentException("The object cannot be null.");
		}
		
		int currentPos = 0;
		Node<E> targetNode = null;
		Node<E> currentNode = this.header;
		if (this.isEmpty() || this.firstIndex(obj) < 0) {
			return false;
		}
		while (currentPos != this.firstIndex(obj)) {
			currentNode = currentNode.getNext();
			currentPos++;
		}
		targetNode =  currentNode.getNext();
		currentNode.setNext(targetNode.getNext());
		targetNode.getNext().setPrev(currentNode);
		targetNode.cleanLinks();
		this.size--;
		return true;
	}
	
	/* Return true if the node at certain index is removed.*/
	@Override
	public boolean remove(int index) {
		return this.remove(this.get(index));
	}

	/* Removes all elements from the list. Returns an integer pertaining 
	 * to the amount of elements removed from the list. 
	 */
	@Override
	public int removeAll(E obj) {
		int count = 0;
		while(this.remove(obj)){
			count++;
		}
		return count;
	}

	/* Returns the first element in the list. If list is empty returns null.*/
	@Override
	public E first() {
		if(this.isEmpty()) {
			return null;
		}
		return this.header.getNext().getElement();
	}

	/* Returns the last element in the list. If list is empty returns null.*/
	@Override
	public E last() {
		if(this.isEmpty()) {
			return null;
		}
		return this.header.getPrev().getElement();
	}

	/* Returns the element in the list at a given index.*/
	@Override
	public E get(int index) {
		if ((index < 0) || (index >= this.size())) {	
			throw new IndexOutOfBoundsException();
		}
		else {
			int count = 0;
			Node<E> currentNode = this.header.getNext() ;
			for (currentNode = this.header.getNext(); count < index; currentNode = currentNode.getNext(), count++);
			return currentNode.getElement();
		}
	}

	/* Clears the list of all its elements.*/
	@Override
	public void clear() {		
		while(!this.isEmpty()) {
			this.remove(0);
		}
	}

	/* Returns true if the list contains at least one instance of e.*/
	@Override
	public boolean contains(E e) {
		return this.firstIndex(e) >= 0;
	}

	/* Returns true if the list size is equal to zero.*/
	@Override
	public boolean isEmpty() {
		return this.size == 0;
	}

	/* Returns the first instance of e in the entire list.*/
	@Override
	public int firstIndex(E e) {
		if (e == null){
			throw new IllegalArgumentException("The element cannot be null.");
		}
		else {
			int count = 0;
			Node<E> currentNode = this.header.getNext();
			while(currentNode != this.header) {
				if(currentNode.getElement().equals(e)) {
					return count;
				}
				currentNode = currentNode.getNext();
				count++;
			}
			return -1; //When element not on list return -1
		}
	}
	
	/* Returns the last instance of e in the entire list.*/
	@Override
	public int lastIndex(E e) {
		if (e == null){
			throw new IllegalArgumentException("The element cannot be null.");
		}
		else {
			int count = this.size-1;
			Node<E> currentNode = this.header.getPrev();
			while(currentNode != this.header){
				if (currentNode.getElement().equals(e)){
					return count;
				}
				currentNode = currentNode.getPrev();
				count--;
			}
			return -1; //Element not on list
		}
	}
	
	@SuppressWarnings("hiding")
	private class Node<E> {
		private E element; 
		private Node<E> prev, next; 

		/*Constructors*/
		public Node(Node<E> p, E e, Node<E> n) { 
			prev = p; 
			next = n; 
			element = e;
		}
		public Node(E e) { 
			this(null, e, null); 
		}
		
		public E getElement() { 
			return element; 
		}
		
		@SuppressWarnings("unused")
		public void setElement(E e) {
			element = e; 
		} 
		public Node<E> getPrev() { 
			return prev; 
		}
		public void setPrev(Node<E> prev) { 
			this.prev = prev; 
		}
		public Node<E> getNext() { 
			return next; 
		}
		public void setNext(Node<E> next) { 
			this.next = next; 
		}
		public void cleanLinks() { 
			prev = next = null; 
			element = null; 
		}
	}

}
