package edu.uprm.cse.datastructures.cardealer;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarList;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;


@Path("/cars")
public class CarManager {
	
	private static CircularSortedDoublyLinkedList<Car> cList = CarList.getInstance();
	
	/**
	 * Method that returns an array of all the cars 
	 * @return carsArr - An array containing all the cars on the list.
	 */
	@GET
//	@Path("")
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getAllCars() {
		Car[] carsArr = new Car[cList.size()];
		if(cList.isEmpty()) {
			return null;
		}
		for(int i=0; i<carsArr.length; i++) {
			carsArr[i] = cList.get(i);
		}
		return carsArr;
	}
	
	/**
	 * Method that returns a specific car on the list
	 * @param id - Id of the specified car.
	 * @return car - Specified car on the list, if not found returns NotFoundException.
	 */
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCar(@PathParam("id") long id) {
		for(int i = 0; i<cList.size(); i++) {
			if(cList.get(i).getCarId() == id) {
				return cList.get(i);
			}
		}
		throw new NotFoundException(); //"404 (Not Found)"
	}  
	
	/**
	 * Method that adds a car to the list (No two cars with the same id).
	 * @param car - Car to be added to the list.
	 * @return Response - Created response if car is added. Forbidden response if id already exist on list.
	 */
	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCar(Car car){
		for(int i=0; i< cList.size(); i++){
			if(cList.get(i).getCarId() == car.getCarId()){
				return Response.status(Response.Status.FORBIDDEN).build(); //Two cars with same id is forbidden
			}	
		}
		cList.add(car);
		return Response.status(201).build();
	} 
	
	/**
	 * Method that updates a specific car on the list
	 * @param car - Car to be updated on the list.
	 * @return Response - OK response if car is updated. Not found response if car not on the list.
	 */
	@PUT
	@Path("/{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCar(Car car){
		int i=0;
		Car cUpdate = car;
		while(cList.get(i).getCarId() != car.getCarId()) {
			i++;
		}
		if(cUpdate == null)
			return Response.status(Response.Status.NOT_FOUND).build(); 
		cList.remove(i);
		cList.add(car);
		return Response.status(Response.Status.OK).build();
	}
	
	/**
	 * Method that deletes a specific car on the list
	 * @param id - Id of the specified car.
	 * @return Response - OK response if car is deleted. Not found response if car not on the list.
	 */
	@DELETE
	@Path("/{id}/delete")
	public Response deleteCar(@PathParam("id") long id){
		for(int i=0; i<cList.size(); i++) {
			if(cList.get(i).getCarId() == id) {
				cList.remove(i);
				return Response.status(Response.Status.OK).build();
			}
		}
		return Response.status(Response.Status.NOT_FOUND).build(); 
	}
}
